# My Managed Hosts

## Requirements

### python3

None

### Ansible

- [geerlingguy.ntp]([https://](https://github.com/geerlingguy/ansible-role-ntp))

## Credits

This package was created with Cookiecutter and the [mkuurstra/cookiecutter-ansible](https://gitlab.com/mkuurstra/cookiecutter-ansible) project template.
