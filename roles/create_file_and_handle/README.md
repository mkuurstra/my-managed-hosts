# create_file_and_handle

Create file and optionally trigger a predefined handler

## Role Variables

- **create_file_and_handle_files**: Dictionary where key is the file path and content describes the following:
    - **content**: Content of the file
    - **owner**: Name of the user that should own the filesystem object
    - **group**: Name of the group that should own the filesystem object
    - **mode**: The permissions of the destination file
    - **handlers** (optional): The name of the handler configured in the playbook

## Dependencies

N.A.

## Example Playbook

```yaml
---
- hosts: all
  vars:
    create_file_and_handle_files:
      /tmp/foo:
        content: |
          This is the file content
        owner: foo
        group: bar
        handlers: [Restart bar]

  handlers:
  - name: Restart bar
    become: true
    ansible.builtin.service:
      name: bar
      state: restarted

  roles:
  - create_file_and_handle
```
