# install_packages

Install packages

## Role Variables

- install_packages_list: list of packages to install

## Dependencies

N.A.

## Example Playbook

```yaml
---
- hosts: all
  vars:
    install_packages_list:
    - vim

  roles:
  - install_packages
```
